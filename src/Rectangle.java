public class Rectangle {
    float length = 1.0f ;
    float width = 1.0f;

    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
    public Rectangle() {
        return ;
    }
    public static double getArea(float length , float width){
        return length * width ;
    }
    public static double getPerimeter(float length , float width) {
        return (length + width) * 2 ; 
    }

    @Override
    public String toString(){
        return "Rectangle [length : " + getPerimeter(this.length, this.width) + " , width : " + getArea(this.length, this.width) + "]" ;
    }
}
